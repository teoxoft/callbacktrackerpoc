<a name="top">
# Webhooks

1. [CallbackCall Events](#callback)
    1. [CallbackCall.Completed Event](#callback_completed)
    2. [CallbackCall.Scheduled Event](#callback_scheduled)
2. [SMS Event](#sms)
3. [Email Event](#email)
4. [Chat dialog Event](#chat)

<a name="callback" />

#### 1. Callback Tracker send two types of CallbackCall Events: 

- `"CallbackCall.Completed"` - triggered when Callback Call has finished
- `"CallbackCall.Scheduled"` - triggered when Callback Call has scheduled by customer and date and time for the call was not specified

<a name="callback_completed" />

#### 1.1 CallbackCall.Completed Event: 

Method:

```
POST
```

Webhook payload:

```json
{
    "event":             "CallbackCall.Completed",
    "status":            "[string]",
    "callbackRequestId": "[int]",
    "widgetId":          "[int]",
    "website":           "[string]",
    "webpage":           "[string]",
    "source":            "[string]",
    "sourceType":        "[string]",
    "campaign":          "[string]",
    "keyword":           "[string]",
    "fromNumber":        "[string]",
    "county":            "[string]",
    "state":             "[string]",
    "city":              "[string]",
    "ip":                "[string]",
    "timestamp":         "[string]",
    "callData": {
        "callId":    "[int]",
        "duration":  "[int]",
        "startedAt": "[string]",
        "endedAt":   "[string]",
        "recording": "[string]",
        "representative": {
            "id":          "[int]",
            "name":        "[string]",
            "phoneNumber": "[string]",
            "email":       "[string]"
        }
    },
    "sfEntity": "[string]"
}
```
Value of the `"status"` field can be:

- `"successful"` - Successful call
- `"missed-by-rep"` - Missed call by representative 
- `"missed-by-customer"` - Missed call by customer  

Fields `"timestamp"`, `"startedAt"`, `"endedAt"` has ISO8601 ('Y-m-d\TH:i:sO') format.

Field `"representative"` is optional

Value of the `"sfEntity"` field can be:

- `"Lead"` - create Lead in SalesForce if needed
- `"Case"` - create Case in SalesForce if needed
- `"Contact"` - create Contact in SalesForce if needed

SalesForce `"Task"` fields mapping:

- "Subject": 

If `"status"` == `"successful"` then "Successful call from `"website"` on `"timestamp"`"

If `"status"` == `"missed-by-rep"` then "Missed call from `"website"` on `"timestamp"`"

If `"status"` == `"missed-by-customer"` then "We tried to call your website visitor, but they didn’t answer"

- "Phone": `"fromNumber"`

- "Comment": 

    Location: `"country"` `"state"` `"city"`
    
    On page: `"webpage"`
    
    IP address: `"ip"`
    
    Source: `"source"`
    
    Source Type: `"sourceType"`
    
    Campaign: `"campaign"`
    
    Keyword `"keyword"`
    
    Recording: `"recording"`

[⇪ top](#top)

<a name="callback_scheduled" />

#### 1.2 CallbackCall.Scheduled Event: 

Method:

```
POST
```

Webhook payload:

```json
{
    "event":             "CallbackCall.Scheduled",
    "status":            "[string]",
    "callbackRequestId": "[int]",
    "widgetId":          "[int]",
    "website":           "[string]",
    "webpage":           "[string]",
    "source":            "[string]",
    "sourceType":        "[string]",
    "campaign":          "[string]",
    "keyword":           "[string]",
    "fromNumber":        "[string]",
    "county":            "[string]",
    "state":             "[string]",
    "city":              "[string]",
    "ip":                "[string]",
    "timestamp":         "[string]",
    "scheduled_at":      "[string]",
    "sfEntity": "[string]"
}
```
Value of the `"status"` field can be:

- `"non-business-hours"` - date and time for the Call was not specified by the customer

Value of the `"sfEntity"` field can be:

- `"Lead"` - create Lead in SalesForce if needed
- `"Case"` - create Case in SalesForce if needed
- `"Contact"` - create Contact in SalesForce if needed

SalesForce `"Task"` fields mapping:

- "Subject": "Visitor requested callback from `"website"` on `"timestamp"`"
- "Phone": `"fromNumber"`
- "Comment": 

    Location: `"country"` `"state"` `"city"`
    
    On page: `"webpage"`
    
    IP address: `"ip"`
    
    Source: `"source"`
    
    Source Type: `"sourceType"`
    
    Campaign: `"campaign"`
    
    Keyword `"keyword"`

[⇪ top](#top)

<a name="sms" />

#### 2. SMS Event: 

Method:

```
POST
```

Webhook payload:

```json
{
    "event":             "Sms",
    "status":            "[string]",
    "callbackRequestId": "[int]",
    "widgetId":          "[int]",
    "website":           "[string]",
    "webpage":           "[string]",
    "source":            "[string]",
    "sourceType":        "[string]",
    "campaign":          "[string]",
    "keyword":           "[string]",
    "fromNumber":        "[string]",
    "county":            "[string]",
    "state":             "[string]",
    "city":              "[string]",
    "ip":                "[string]",
    "timestamp":         "[string]",
    "smsMessage":        "[string]",
    "sfEntity":          "[string]"
}
```
Value of the `"status"` field can be:

- `"delivered"` - information successfully delivered to representatives
- `"undelivered"` - information was not delivered to representatives

Field `"smsMessage"`contains text of the message

Value of the `"sfEntity"` field can be:

- `"Lead"` - create Lead in SalesForce if needed
- `"Case"` - create Case in SalesForce if needed
- `"Contact"` - create Contact in SalesForce if needed

SalesForce `"Task"` fields mapping:

- "Subject": "Text Message Request from `"website"` on `"timestamp"`"
- "Phone": `"fromNumber"`
- "Comment": 

    Location: `"country"` `"state"` `"city"`
    
    On page: `"webpage"`
    
    IP address: `"ip"`
    
    Source: `"source"`
    
    Source Type: `"sourceType"`
    
    Campaign: `"campaign"`
    
    Keyword `"keyword"`
    
    `"smsMessage"`

[⇪ top](#top)

<a name="email" />

#### 3. Email Event: 

Method:

```
POST
```

Webhook payload:

```json
{
    "event":             "Email",
    "status":            "[string]",
    "callbackRequestId": "[int]",
    "widgetId":          "[int]",
    "website":           "[string]",
    "webpage":           "[string]",
    "source":            "[string]",
    "sourceType":        "[string]",
    "campaign":          "[string]",
    "keyword":           "[string]",
    "fromEmail":         "[string]",
    "county":            "[string]",
    "state":             "[string]",
    "city":              "[string]",
    "ip":                "[string]",
    "timestamp":         "[string]",
    "emailMessage":      "[string]",
    "sfEntity":          "[string]"
}
```
Value of the `"status"` field can be:

- `"delivered"` - information successfully delivered to representatives
- `"undelivered"` - information was not delivered to representatives

Field `"emailMessage"` contains text of the message

Value of the `"sfEntity"` field can be:

- `"Lead"` - create Lead in SalesForce if needed
- `"Case"` - create Case in SalesForce if needed
- `"Contact"` - create Contact in SalesForce if needed

SalesForce `"Task"` fields mapping:

- "Subject": "Visitor sent E-mail message from `"website"` on `"timestamp"`"
- "Phone": `"fromNumber"`
- "Email": `"Email"`
- "Comment": 

    Location: `"country"` `"state"` `"city"`
    
    On page: `"webpage"`
    
    IP address: `"ip"`
    
    Source: `"source"`
    
    Source Type: `"sourceType"`
    
    Campaign: `"campaign"`
    
    Keyword `"keyword"`
    
    `"emailMessage"`

[⇪ top](#top)

<a name="chat" />

#### 4. Chat dialog Event: 

Method:

```
POST
```

Webhook payload:

```json
{
    "event":        "Chat",
    "status":       "[string]",
    "chatId":       "[int]",
    "widgetId":     "[int]",
    "website":      "[string]",
    "webpage":      "[string]",
    "source":       "[string]",
    "sourceType":   "[string]",
    "campaign":     "[string]",
    "keyword":      "[string]",
    "visitorEmail": "[string]",
    "county":       "[string]",
    "state":        "[string]",
    "city":         "[string]",
    "ip":           "[string]",
    "timestamp":    "[string]",
    "dialogData": {
         "sid":         "[string]",
         "transcripts": "[string]",
         "representative": {
            "id":          "[int]",
            "name":        "[string]",
            "phoneNumber": "[string]",
            "email":       "[string]"
         }
    },
    "sfEntity": "[string]"
}
```
Value of the `"status"` field can be:

- `"assigned"` - chat dialog assigned to representative
- `"missed"` - chat dialog missed by representatives

Field `"transcripts"` contains transcripts of the chat dialog with line breaks, for example:

```
Chatter (test@test.com)
09:43 | Hi there
09:44 | I want to know how transcripts will look in my CRM

John (john@company.com)
09:44 | Sure thing!
09:44 | Let's take a look

```

Value of the `"sfEntity"` field can be:

- `"Lead"` - create Lead in SalesForce if needed
- `"Case"` - create Case in SalesForce if needed
- `"Contact"` - create Contact in SalesForce if 

SalesForce `"Task"` fields mapping:

- "Subject": "Live Chat from `"website"` on `"timestamp"`"
- "Email": `"visitorEmail"`
- "Comment": 
       
    Location: `"country"` `"state"` `"city"`
       
    On page: `"webpage"`
       
    IP address: `"ip"`
       
    Source: `"source"`
       
    Source Type: `"sourceType"`
       
     Campaign: `"campaign"`
       
    Keyword `"keyword"`
    
    Talked with: `"representative"`.`"name"`
    
    For a full transcript, please see: `"sid"`
    
    `"transcripts"`

[⇪ top](#top)
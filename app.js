const express = require('express');
const request = require('request');
const app = express();
const port = 3000;

const CLIENT_ID = '3MVG9wEVwV0C9ejCWy7GbuO1dJHf.wF70a64z_kUJk05i0TJQ.eZ1yMzO504J5gGB1Rrvnj3FfU4EKjhmr6i9';
const CLIENT_SECRET = '33EAA4BA82CEF3673A5431C48DFF2824EC859BFF66FDC3B8B3E89D5B123CA892';
const REDIRECT_URI = 'http://localhost:3000/oauth2/callback';

app.use(express.static('public'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

// Редирект пользователя на страницу логина
app.get('/login', (req, res) => authorize(res));
function authorize(res) {
  res.redirect('https://login.salesforce.com/services/oauth2/authorize?client_id=' + CLIENT_ID + '&redirect_uri=' + REDIRECT_URI + '&response_type=code');
}

// Callback от Salesforce после успешного логина, в url приходит authorization code
app.get('/oauth2/callback', (req, res) => token(req, res));
function token(req, res) {
  const code = req.query.code;
  console.log('authorization_code: ' + code);

  // Получение Access Token и Refresh Token c помощью Authorization Code
  request.post({uri: 'https://login.salesforce.com/services/oauth2/token', form: {
    grant_type: 'authorization_code',
    code: code,
    client_id: CLIENT_ID,
    client_secret: CLIENT_SECRET,
    redirect_uri: REDIRECT_URI
  }}, function(err, httpResponse, body) {
    console.log('body: ' + body);
    let bodyParsed = JSON.parse(body);

    let accessToken = bodyParsed.access_token;
    let instanceUrl = bodyParsed.instance_url;
    let refreshToken = bodyParsed.refresh_token;

    res.redirect('/?accessToken=' + accessToken + '&refreshToken=' + refreshToken + '&instanceUrl=' + instanceUrl);
  });
}

app.get('/getAccounts', (req, res) => {
  let instanceUrl = req.query.instanceUrl;
  let accessToken = req.query.accessToken;
  request.get({uri: instanceUrl + '/services/data/v48.0/query/?q=SELECT+Name+From+Account'}, function(err, httpResponse, body) {
    console.log('Accounts: ');
    console.log(body);

    res.send(body);
  }).auth(null, null, true, accessToken);
});

app.get('/getAccountsWithSfRestApi', (req, res) => {
  let instanceUrl = req.query.instanceUrl;
  let accessToken = req.query.accessToken;
  request.get({uri: instanceUrl + '/services/apexrest/Accounts'}, function(err, httpResponse, body) {
    console.log('Accounts: ');
    console.log(body);

    res.send(body);
  }).auth(null, null, true, accessToken);
})

// Получение Access Token с помощью Refresh Token
app.get('/getAccessTokenUsingRefreshToken', (req, res) => getTokenUsingRefreshToken(req, res));
function getTokenUsingRefreshToken(req, res) {
  let refreshToken = req.query.refreshToken;

  request.post({uri: 'https://login.salesforce.com/services/oauth2/token', form: {
      grant_type: 'refresh_token',
      refresh_token: refreshToken,
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET
    }}, function(err, httpResponse, body) {
    console.log('body: ' + body);
    let bodyParsed = JSON.parse(body);

    let accessToken = bodyParsed.access_token;
    let instanceUrl = bodyParsed.instance_url;

    res.redirect('/?accessToken=' + accessToken + '&refreshToken=' + refreshToken + '&instanceUrl=' + instanceUrl);
  });
}

app.get('/updateRefreshToken', (req, res) => {
  let instanceUrl = null;
  let accessToken = null;
  let refreshToken = req.query.newRefreshToken;

  res.redirect('/?accessToken=' + accessToken + '&refreshToken=' + refreshToken + '&instanceUrl=' + instanceUrl);
})